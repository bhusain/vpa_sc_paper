\begin{thebibliography}{10}

\bibitem{LULESH}
{H}ydrodynamics {C}hallenge {P}roblem, {L}awrence {L}ivermore {N}ational
  {L}aboratory.
\newblock Technical Report LLNL-TR-490254.

\bibitem{adamsd}
M.~Adams et~al.
\newblock {\em Package for AMR Applications - Design Document, Lawrence
  Berkeley National Laboratory Technical Report LBNL-6616E.}

\bibitem{Gimenez:2014:DOM:2683593.2683612}
Alfredo Gim{\'e}nez, Todd Gamblin, Barry Rountree, Abhinav Bhatele, Ilir
  Jusufi, Peer-Timo Bremer, and Bernd Hamann.
\newblock Dissecting on-node memory access performance: A semantic approach.
\newblock In {\em Proceedings of the International Conference for High
  Performance Computing, Networking, Storage and Analysis}, SC '14, pages
  166--176, Piscataway, NJ, USA, 2014. IEEE Press.

\bibitem{childs2013visit}
Hank Childs.
\newblock Visit: An end-user tool for visualizing and analyzing very large
  data.
\newblock 2013.

\bibitem{Rutar20122}
Nick Rutar and Jeffrey~K. Hollingsworth.
\newblock Data centric techniques for mapping performance data to program
  variables.
\newblock {\em Parallel Computing}, 38(1–2):2 -- 14, 2012.
\newblock Extensions for Next-Generation Parallel Programming Models.

\bibitem{6047189}
M.~Schulz, J.A. Levine, P.-T. Bremer, T.~Gamblin, and V.~Pascucci.
\newblock Interpreting performance data across intuitive domains.
\newblock In {\em Parallel Processing (ICPP), 2011 International Conference
  on}, pages 206--215, Sept 2011.

\bibitem{bohme2014characterizing}
David B{\"o}hme.
\newblock {\em Characterizing load and communication imbalance in parallel
  applications}, volume~23.
\newblock Forschungszentrum J{\"u}lich, 2014.

\bibitem{Huck:2014:LPD:2688457.2688467}
Kevin~A. Huck, Kristin Potter, Doug~W. Jacobsen, Hank Childs, and Allen~D.
  Malony.
\newblock Linking performance data into scientific visualization tools.
\newblock In {\em Proceedings of the First Workshop on Visual Performance
  Analysis}, VPA '14, pages 50--57, Piscataway, NJ, USA, 2014. IEEE Press.

\bibitem{tau}
S.~Shende and A.~D. Malony.
\newblock The tau parallel performance system.
\newblock In {\em International Journal of High Performance Computing
  Applications}, vol. 20, page 287?311, 2006.

\bibitem{MPAS}
Lanl and ncar.
\newblock MPAS, http://mpas-dev.github.io.

\bibitem{CONDUIT}
Conduit.
\newblock Technical Report LLNL-CODE-666778.

\end{thebibliography}
